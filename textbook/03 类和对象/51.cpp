/*
    类中的成员默认是私有的，用关键词 class 表示类，C++ 结构体中的成员可以分为公有和私有，默认情况下，
    结构体中的成员是公有的。

类类型声明的一般形式如下：
    class 类名
    {
        private:
            私有数据成员和成员函数

        public:
            共有数据成员和成员函数
    }
*/

#include <iostream>
#include <cmath>
using namespace std;

class Complex // 声明了一个名为 Complex 的类
{
private: // 声明以下部分为私有的
    double real;
    double imag;

public: // 声明以下部分为公有的
    void init(double r, double i)
    {
        real = r;
        imag = i;
    }
    double abscomplex()
    {
        double t;
        t = real * real + imag * imag;
        return sqrt(t); // 返回 t 的开方
    }
};

int main()
{
    Complex A;
    A.init(1.1, 2.2); // 类外的对象 A 可以访问公有成员函数 init

    cout << "复数的绝对值是 : " << A.abscomplex() << endl;
    // 类外的对象 A 可以访问公有成员函数 abscomplex

    return 0;
}