/*
    类的成员函数是函数的一种，成员函数可以访问本类中的任何成员，包括公有私有、保护的成员
    在类外定义成员函数时，必须在成员函数名之前加上类名，在类名和函数名之间应加上作用域运算符 "::" ,
    用于声明这个成员函数是属于哪个类的
*/

#include <iostream>
using namespace std;

class Point
{
public:
    void setpoint(int, int); //设置坐标点的成员函数 setpoint 的函数原型
    int getx();              // 取 x 坐标点的成员函数
    int gety();              // 取 y 坐标点的成员函数

private:
    int x, y;
};

void Point ::setpoint(int a, int b) // 在类外定义成员函数 setpoint
{
    x = a;
    y = b;
}

int Point ::getx() // 在类外定义成员函数 getx
{
    return x;
}

int Point ::gety() // 在类外定义成员函数 gety
{
    return y;
}
