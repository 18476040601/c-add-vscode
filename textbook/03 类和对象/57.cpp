/*
    通过对象名和对象选择符访问对象中的成员
*/

#include <iostream>
using namespace std;

class Point
{
public:
    void setpoint(int, int); //设置坐标点的成员函数 setpoint 的函数原型
    int getx();              // 取 x 坐标点的成员函数
    int gety();              // 取 y 坐标点的成员函数

private:
    int x, y;
};

void Point ::setpoint(int a, int b) // 在类外定义成员函数 setpoint
{
    x = a;
    y = b;
}

int Point ::getx() // 在类外定义成员函数 getx
{
    return x;
}

int Point ::gety() // 在类外定义成员函数 gety
{
    return y;
}

int main()
{
    Point op1, op2; // 定义对象 op1 和 op2
    int i, j;

    op1.setpoint(1, 2); // 调用对象 op1 的成员函数 setpoint，给 op1 的数据成员赋值
    op2.setpoint(3, 4); // 调用对象 op2 的成员函数 setpoint，给 op2 的数据成员赋值

    i = op1.getx(); // 调用对象 op1 的成员函数 getx , 取 op1 的 x 值
    j = op1.gety(); // 调用对象 op1 的成员函数 gety , 取 op1 的 y 值
    cout << "op1 i = " << i << "   op1 j = " << j << endl;

    i = op2.getx(); // 调用对象 op2 的成员函数 getx , 取 op2 的 x 值
    j = op2.gety(); // 调用对象 op2 的成员函数 gety , 取 op2 的 y 值
    cout << "op2 i = " << i << "   op2 j = " << j << endl;

    return 0;
}
