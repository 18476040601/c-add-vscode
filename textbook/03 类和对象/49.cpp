/**************************结构体的扩充*******************************/
/*
    用扩充的结构体类型求复数的绝对值
*/

#include <iostream>
#include <cmath>
using namespace std;

struct Complex   // 声明了一个名为 Complex 的结构体
{
    double real;
    double imag;

    void init (double r, double i)
    {
        real = r;
        imag = i;
    }
    double abscomplex()
    {
        double t;
        t = real * real + imag * imag;
        return sqrt(t); // t 的开方
    }
};

int main()
{
    Complex A;
    A.init (1.1, 2.2);

    cout << "复数的绝对值是 : " << A.abscomplex() << endl;

    return 0;
}