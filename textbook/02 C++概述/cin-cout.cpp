#include <iostream>

using namespace std;

int main()
{
    char name[20];

    cout << "Hello , your name : " << endl; // endl 相当于换行符，结束一行的标志，输出
    cin >> name;                            // 从cin 输入流获得数据给name，输入

    cout << "My name is " << name << '\n' ; // '\n' 可换为 endl

    return 0;
}