/*
    如果有两个同名变量，局部变量在其作用域内具有较高的优先权
    如果希望在局部变量的作用域内使用同名的全局变量，可以在变量前面加上 "::" 
*/

#include <iostream>

using namespace std;

int aver;

int main()
{
    int aver = 25; // 给局部变量aver赋值
    ::aver = 10;   // 给全局变量aver赋值

    cout << "local  aver = " << aver << endl;    // 输出局部变量aver的值
    cout << "global aver = " << ::aver << endl; // 输出全局变量aver的值

    return 0;
}