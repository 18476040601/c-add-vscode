// 使用引用返回函数值

#include <iostream>
using namespace std;

int a[] = {1, 3, 5, 7, 9};

int &index(int i); // 声明函数返回一个整型类型的引用

int main()
{
    cout << index(2) << endl; // 等价于输出 a[2]

    index(2) = 25;            // 将函数调用放在赋值左边，即a[2] 赋值为25
    cout << index(2) << endl; // 等价于输出 a[2]

    return 0;
}

int &index(int i)
{
    return a[i]; // 定义函数返回一个整型的引用，等价于返回数组元素 a[i]
}