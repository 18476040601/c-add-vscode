/*
    int i = 5 ;
    int &j = i ; // j 是变量 i 的引用
    "&" 是引用声明符，此时它不代表地址。对变量声明一个引用，并不另外开辟内存单元，
    变量i 和引用j 占用内存的同一位置。当i变化时，j 也随之变化，反之也一样 。
*/

#include <iostream>
using namespace std;

int main()
{
    int i;
    int &j = i; // 声明 j 是一个整型变量 i 的引用
    i = 30;
    cout << "i = " << i << "  j = " << j << '\n'; // 输出变量 i 和引用 j 的值
    j = 80;
    cout << "i = " << i << "  j = " << j << '\n'; // 输出变量 i 和引用 j 的值
    cout << "变量 i 的地址：" << &i << '\n';    // 输出变量 i 的地址
    cout << "引用 j 的地址：" << &j << '\n';    // 输出引用 j 的地址

    return 0;
}