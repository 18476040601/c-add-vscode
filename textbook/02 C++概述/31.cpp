/*
    C++支持函数重载，函数名可以相同，只要函数参数的类型不同，或者参数的个数不同。
    例如：求不同类型参数的平方数。
    square(int i) ;  square (double i) ; square (int i , int j) ; 都是合理的
*/

#include <iostream>

using namespace std ;

int mul (int x , int y)  // 函数
{
    return x * y ;
}

int mul (int x , int y , int z)
{
    return x * y * z ;
}

int main()
{
    int a = 3 , b = 4 , c = 5 ;
    cout << a << " * "  << b << " = " << mul (a , b) << endl ;
    cout << a << " * " << b << " * " << c << " = " << mul (a , b , c) << endl ;    

    return 0 ;
}
