/************************* 应用引用的综合例子********************/

#include <iostream>
using namespace std;

int &max(int &num1, int &num2)
{
    return (num1 > num2) ? num1 : num2;
}

int &min(int &num1, int &num2)
{
    return (num1 < num2) ? num1 : num2;
}

int main()
{
    int num1, num2;

    /*********************最大数*********************************/
    cout << "请输入第 1 个数 ：";
    cin >> num1;
    cout << "请输入第 2 个数 ：";
    cin >> num2;

    max(num1, num2) = 0;

    cout << "\n找出最大数，然后把最大数赋值为 0 后，两个数分别为 : " << endl;
    cout << "num1 = " << num1 << "  num2 = " << num2 << endl;

    /****************************最小数**********************************/
    cout << "现在请再输入两个数 : " << endl;
    cout << "请输入第 1 个数 ：";
    cin >> num1;
    cout << "请输入第 2 个数 ：";
    cin >> num2;

    min(num1, num2) = 0;

    cout << "\n找出最小数，然后把最小数赋值为 0 后，两个数分别为 : " << endl;
    cout << "num1 = " << num1 << "  num2 = " << num2 << "\n\n";

    return 0;
}