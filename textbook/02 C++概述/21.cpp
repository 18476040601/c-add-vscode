#include <iostream>

using namespace std;

int main()
{
    int x = 25;

    cout << "分别代表十六进制，十进制以及八进制的 X ，X = 25 " << endl ;

    cout << hex << x << ' ' << dec << x << ' ' << oct << x << endl;

    return 0;
}