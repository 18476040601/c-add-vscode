/*
    比较引用和指针的使用方法
*/

#include <iostream>
using namespace std;

int main()
{
    int i = 15;
    int *iptr = &i; // 定义指针型变量 iptr ，将变量 i 的地址赋给 iptr
    int &rptr = i;  // 声明变量 i 的引用 rptr ，rptr 是变量 i 的别名

    cout << "i = " << i << endl;
    cout << "*iptr = " << *iptr << endl;
    cout << "rptr  = " << rptr << endl;

    i = 29;

    cout << "\nAfter changing i to 29 : \n" << endl;
    cout << "i = " << i << endl;
    cout << "*iptr = " << *iptr << endl;
    cout << "rptr  = " << rptr << endl;

    return 0;
}