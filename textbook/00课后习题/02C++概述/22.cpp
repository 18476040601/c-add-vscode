/*
    用动态分配空间的方法计算 Fibonacci 数列的前20项，并存储到动态分配的空间中
*/

#include <iostream>
using namespace std;

#define N 20

int main()
{
    int *pi = new int[20];
    *pi = 1; // pi[0] = 1
    pi[1] = 1;

    for (int i = 2; i < N; i++)
    {
        pi[i] = pi[i - 2] + pi[i - 1];
    }

    cout << pi[N - 1] << endl ;

    return 0;
}