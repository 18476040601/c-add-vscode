#include <iostream>
using namespace std;

int &f(int &i)
{
    i += 10;
    return i;
}

int main()
{
    int k = 0;
    int &m = f(k);
    cout << k << endl; // 输出 k 为10

    m = 20;
    cout << k << endl; // 输出 k 为20

    return 0;
}