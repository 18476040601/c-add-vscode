/*
常见的几种打开方式
    ios::in--打开一个可读取文件
    ios::out --打开一个可写入文件
    ios::binary --以二进制的形式打开一个文件。
    ios::app --写入的所有数据将被追加到文件的末尾
    ios::trunk -- f删除文件原来已存在的内容
    ios::nocreate --如果要打开的文件并不存在，那么以此参数调用open函数将无法进行。
    ios::noreplece --如果要打开的文件已存在，试图用open函数打开时将返回一个错误。
*/

#include <fstream>
#include <iostream>

using namespace std ;

int main()
{
    ofstream out ; 

    out.open("test.txt" , ios::app ) ;
    if( !out )
    {
        cerr << "打开文件失败！" << endl ;
        return 0 ;
    }

    for (int i = 0 ; i < 10 ; i++)
    {
        out << i ;
    }

    out << endl ;
    out.close() ;

    return 0 ;
}