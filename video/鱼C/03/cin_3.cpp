#include <iostream>

using namespace std;

int main()
{
    const int SIZE = 50 ;
    char buf[SIZE] ;

    cout << "请输入一段文本 ：\n" ;
    cin.read (buf , 20) ; // 读取20个字节

    cout << "字符串收到的字符数为：" << cin.gcount() << endl ; // 计算提取到的字符数

    cout << "输入的文本信息时：" ;
    cout.write(buf , 20) ;  // 输出提取到buf中的20个字符
    cout << endl ;

    return 0;
}