#include <iostream>

using namespace std;

int main()
{
    char p;

    cout << "请输入一段文本 : \n";

    while (cin.peek() != '\n') // cin.peek 表示从输入的字符中逐渐筛选，如果字符不等于回车，为真
    {
        p = cin.get(); // 获取字符赋给p
        cout << p ;
    }

    cout << endl;

    return 0;
}