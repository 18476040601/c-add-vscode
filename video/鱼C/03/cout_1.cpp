#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    double n;

    cout << "请输入一个你要开方的数：" << endl;
    cin >> n;

    cout << "对于" << n << "开平方的求解：" << endl;

    double value = sqrt(n);
    cout << value << endl;

    for (int i = 1; i <= 9; i++)
    {
        /*
        cout.precision()其实是输出流cout的一个格式控制函数，也就是在iostream中的一个成员函数。
        precision()返回当前的浮点数的精度值，而cout.precision(val)其实就是在输出的时候设定输出值
        以新的浮点数精度值显示，即小数点后保留val位。
        */
        cout.precision(i);
        cout << value << endl;
    }

    cout << "输出的数的精度为" << cout.precision() << endl ;

    return 0;
}
