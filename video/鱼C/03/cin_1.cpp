#include <iostream>

using namespace std;

int main()
{
    char buf[20];

    cin.ignore(7);        // 忽略自左向右的6位字符（包括空格）
    cin.getline(buf, 10); // 获取被忽略后的buf的10位字符，最后一个位结束字符'\0',即获取的9个字符和结束符

    cout << buf << endl;

    return 0;
}