#include <iostream>

// using namespace std;

int main()
{
    char answer;

    std::cout << "请问可以格式化你的硬盘吗，请输入 Y/N :";
    std::cin >> answer; // :: 表示作用域运算符，在std作用域内使用

    if (answer == 'Y' || answer == 'y' || answer == 'N' || answer == 'n')
    {
        if (answer == 'Y' || answer == 'y')
        {
            std::cout << "你选择的是" << answer << ", 格式化硬盘" << std::endl;
        }
        if (answer == 'N' || answer == 'n')
        {
            std::cout << "你选择的是" << answer << ", 不格式化硬盘" << std::endl;
        }
    }

    else
    {
        std::cout << "你的输入有误，请输入Y/N或y/n" << std::endl;
    }

    std::cin.ignore(100, '\n'); // 忽略100 个字符，直到遇到回车字符
                                // 常用功能就是用来清除以回车结束的输入缓冲区的内容

    std::cout << "请输入任意一个字符以退出" << "\n";
    std::cin.get();

    return 0;
}