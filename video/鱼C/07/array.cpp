#include <iostream>

int main()
{
    const unsigned short NUM = 10; // 设置常态变量

    int a[NUM];
    int sum = 0;

    std::cout << "请输入" << NUM << "个任意的整数\n\n";
    for (int i = 0; i < NUM; i++)
    {
        std::cout << "请输入第" << i + 1 << "个数据 : ";

        while ( !(std::cin >> a[i] ) ) // 检验输入的是否合法
        {
            std::cin.clear() ;
            std::cin.ignore (100 , '\n') ; // 清除存放在缓冲区的换行键
            std::cout << "请重新输入一个合法的值 : " ;
        }
        sum += a[i];
    }

    std::cout << "\n这10个数的和为 sum = " << sum << '\n';
    std::cout << "\n这10个数的平均值为 aver = " << (float)sum / NUM << "\n\n";

    return 0;
}