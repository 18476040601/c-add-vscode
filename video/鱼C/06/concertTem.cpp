/*
    温度单位转换器，如果输入xx.xC 为摄氏度，xx.xF 为华氏度
    华氏度 == 摄氏度 * 9.0/5.0 + 32 
    摄氏度 == (华氏度 - 32) ÷ 1.8
    函数重载
*/

#include <iostream>

void ConvertTemperature(double tempIn, char typeIn); // 函数声明
void ConvertTemperature(int tempIn, char typeIn);

int main()
{
    double tempIn;
    int tempInInt;
    char typeIn;

    std::cout << "请输入以【xx.x C】或【xx.x F】的格式的温度 : ";
    std::cin >> tempIn >> typeIn; // 如果输入33.2 C ,则33.2给tempIn,空格表示输入下一个,则C给typeIn
    std::cin.ignore(100, '\n');   // 吸收缓冲区的换行符
    std::cout << '\n';
    ConvertTemperature(tempIn, typeIn);

    std::cout << "请输入以【xx C】或【xx F】的格式的温度 : ";
    std::cin >> tempInInt >> typeIn; // 如果输入33 C ,则33给tempIn,空格表示输入下一个,则C给typeIn
    std::cin.ignore(100, '\n');   // 吸收缓冲区的换行符
    std::cout << '\n';
    ConvertTemperature(tempInInt, typeIn);

    std::cout << "请输入任意一个字符以退出"
              << "\n";
    std::cin.get();

    return 0;
}

void ConvertTemperature(double tempIn, char typeIn)
{
    const unsigned short ADD_SUBTRACT = 32; // 定义静态变量无符号短整型 ADD_SUBTRACT (加减) 变量
    const double RATIO = 9.0 / 5.0;         // 定义静态变量 RATIO (比率) 变量

    double tempOut;
    char typeOut;

    switch (typeIn)
    {
    case 'C':
    case 'c':
        tempOut = tempIn * RATIO + ADD_SUBTRACT;
        typeOut = 'F';
        std::cout << tempIn << " C = " << tempOut << " F"
                  << "\n\n";
        break;

    case 'F':
    case 'f':
        tempOut = (tempIn - ADD_SUBTRACT) / RATIO;
        typeOut = 'C';
        std::cout << tempIn << " F = " << tempOut << " C"
                  << "\n\n";
        break;

    default:
        std::cout << "请输入正确的温度符号"
                  << "\n\n";
        break;
    }
}

void ConvertTemperature(int tempIn, char typeIn)
{
    const unsigned short ADD_SUBTRACT = 32; // 定义静态变量无符号短整型 ADD_SUBTRACT (加减) 变量
    const double RATIO = 9.0 / 5.0;         // 定义静态变量 RATIO (比率) 变量

    int tempOut;
    char typeOut;

    switch (typeIn)
    {
    case 'C':
    case 'c':
        tempOut = tempIn * RATIO + ADD_SUBTRACT;
        typeOut = 'F';
        std::cout << tempIn << " C = " << tempOut << " F"
                  << "\n\n";
        break;

    case 'F':
    case 'f':
        tempOut = (tempIn - ADD_SUBTRACT) / RATIO;
        typeOut = 'C';
        std::cout << tempIn << " F = " << tempOut << " C"
                  << "\n\n";
        break;

    default:
        std::cout << "请输入正确的温度符号"
                  << "\n\n";
        break;
    }
}