#include <iostream>

using namespace std;

void swap(int a, int b)
{
	cout << "交换前 : " << endl;
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;

	// 交换
	int temp = a;
	a = b;
	b = temp;

	cout << "交换后 : " << endl;
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;

}

int SWAP()
{
	int a = 10;
	int b = 20;

	cout << "a = " << a << endl;
	cout << "b = " << b << endl;

	// 当我们做值传递的时候 ， 函数的形参发生改变，并不会影响实参
	swap(a, b);

	cout << "a = " << a << endl;
	cout << "b = " << b << endl;

	system("pause");

	return 0;
}