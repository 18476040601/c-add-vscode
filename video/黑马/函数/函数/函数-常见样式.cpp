#include <iostream> 

using namespace std;

// 函数常见样式 
// 1.无参无返
void test_1()
{
	cout << "this is test_1" << endl;
}

// 2.有参无返
void test_2(int a)
{
	cout << "this is test_2 a = " << a << endl;
}

// 3.无参有返
int test_3()
{
	cout << "this is test_3" << endl;
	return 200;
}

// 4.有参有返
int test_4(int b)
{
	cout << "this is test_4 b = " << b << endl;
	return b;
}

int TEST()
{
	// 无参无返函数调用
	test_1();

	// 有参无返函数调用
	test_2(100);

	// 无参有返函数调用
	test_3();

	// 有参有返函数调用
	test_4(300);

	system("pause");

	return 0;
}