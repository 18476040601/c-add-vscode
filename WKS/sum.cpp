#include <iostream>

using namespace std;

int addArray(int *array, int n);

int main()
{
    int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int size = sizeof(array) / sizeof(array[0]); // 求数组的长度，一共有多少个数

    cout << "结果是 ：" << addArray(array, size) << endl;

    return 0;
}

int addArray(int *array, int n)
{
    int sum = 0;

    for (int i = 0; i < 10; i++)
    {
        sum += *array++;
    }

    return sum;
}
