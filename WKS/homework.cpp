/*
    要求：编写一个程序，要求用户输入一串整数和任意数目的空格，这些整数必须在同一行中，但允许出现在该行
    中的任何位置。当用户按下键盘中的“Enter” 键时，数据输入结束，程序自动对所有整数进行求和并打印出结果。
*/

#include <iostream>

using namespace std ;

int main()
{
    int sum = 0 ;

    cout << "请输入一串整数和任意数目的空格：" ;

    int i ;
    while (cin >> i)
    {
        sum += i ;
        while ( cin.peek() == ' ')
        {
            cin.get() ;
        }
        if( cin.peek() == '\n')
        {
            break;
        }
    }

    cout << "结果是：" << sum << endl ;

    return 0 ;
    
}